To install the tools on a Linux machine, use,
```
make remote
```
To use Vagrant to create a VM locally with the required tools installed, run,
```
make local
```

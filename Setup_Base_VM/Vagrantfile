# Provision a local VM for local component testing and local integration testing.

#Usage:
# vagrant up ( arm | intel )

$ARM_BOX = "rockylinux/9"
$INTEL_BOX = "rockylinux/9"

# See https://jdk.java.net/
$provision_java23_arm = <<-PROVISION_JAVA23_ARM
curl https://download.java.net/java/GA/jdk23.0.1/c28985cbf10d4e648e4004050f8781aa/11/GPL/openjdk-23.0.1_linux-aarch64_bin.tar.gz -o jdk.tar.gz
tar xvf jdk.tar.gz
rm jdk.tar.gz
echo 'export JAVA_HOME=/home/vagrant/jdk-23.0.1' >> /home/vagrant/.bash_profile
export JAVA_HOME=/home/vagrant/jdk-23.0.1
echo 'export PATH=${JAVA_HOME}/bin:${PATH}' >> /home/vagrant/.bash_profile
#sudo yum install -y java
PROVISION_JAVA23_ARM

$provision_java23_intel = <<-PROVISION_JAVA23_INTEL
curl https://download.java.net/java/GA/jdk23.0.1/c28985cbf10d4e648e4004050f8781aa/11/GPL/openjdk-23.0.1_linux-x64_bin.tar.gz -o jdk.tar.gz
tar xvf jdk.tar.gz
rm jdk.tar.gz
echo 'export JAVA_HOME=/home/vagrant/jdk-23.0.1' >> /home/vagrant/.bash_profile
export JAVA_HOME=/home/vagrant/jdk-23.0.1
echo 'export PATH=${JAVA_HOME}/bin:${PATH}' >> /home/vagrant/.bash_profile
#sudo yum install -y java
PROVISION_JAVA23_INTEL

$provision_git = <<-PROVISION_GIT
sudo yum install -y git
PROVISION_GIT

$provision_wget = <<-PROVISION_WGET
sudo yum install -y wget
PROVISION_WGET

$provision_docker = <<-PROVISION_DOCKER
# https://docs.docker.com/engine/install/centos/
sudo yum remove docker docker-client docker-client-latest docker-common docker-latest docker-latest-logrotate docker-logrotate docker-engine
sudo yum install -y yum-utils device-mapper-persistent-data lvm2
sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
sudo yum install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
sudo systemctl start docker
sudo groupadd -f docker
sudo usermod -aG docker vagrant
sudo docker run hello-world
PROVISION_DOCKER

$provision_compose = <<-PROVISION_COMPOSE
# https://docs.docker.com/compose/install/linux/#install-using-the-repository
sudo yum install -y docker-compose-plugin
sudo docker compose version
PROVISION_COMPOSE

$provision_maven = <<-PROVISION_MAVEN
wget https://dlcdn.apache.org/maven/maven-3/3.9.9/binaries/apache-maven-3.9.9-bin.tar.gz
tar xvf apache-maven-3.9.9-bin.tar.gz
rm apache-maven-3.9.9-bin.tar.gz
echo 'export PATH=/home/vagrant/apache-maven-3.9.9/bin:${PATH}' >> /home/vagrant/.bash_profile
./apache-maven-3.9.9/bin/mvn -v
mkdir -p /home/vagrant/.m2
chown -R vagrant:vagrant /home/vagrant/.m2
wget https://gitlab.com/cliffbdf/devops-1/-/raw/master/toolchain-vm.xml
cp toolchain-vm.xml /home/vagrant/.m2/toolchains.xml
PROVISION_MAVEN

$provision_java_utils = <<-PROVISION_JAVA_UTILS
git clone https://gitlab.com/cliffbdf/utilities-java.git
sudo chown -R vagrant:vagrant utilities-java
cd utilities-java
make install
cd
PROVISION_JAVA_UTILS

$provision_hello_cdk = <<-PROVISION_HELLO_CDK
wget https://gitlab.com/cliffbdf/devops-7-hello/-/raw/master/cdk/target/hello.cdk-1.0.jar
mvn install:install-file -Dfile=hello.cdk-1.0.jar -DgroupId=com.cliffberg.devopsforagilecoaches -DartifactId=hello.cdk -Dversion=1.0 -Dpackaging=jar
PROVISION_HELLO_CDK

$provision_unzip = <<-PROVISION_UNZIP
sudo yum install -y unzip
PROVISION_UNZIP

$provision_aws_arm = <<-PROVISION_AWS_ARM
curl "https://awscli.amazonaws.com/awscli-exe-linux-aarch64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
sudo ./aws/install
rm awscliv2.zip
PROVISION_AWS_ARM

$provision_aws_intel = <<-PROVISION_AWS_INTEL
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
sudo ./aws/install
rm awscliv2.zip
PROVISION_AWS_INTEL

$provision_nvm = <<-PROVISION_NODE
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.40.1/install.sh | bash
PROVISION_NODE

$provision_node = <<-PROVISION_NODE
nvm install 22
PROVISION_NODE

$provision_cdk = <<PROVISION_CDK
npm install -g aws-cdk
PROVISION_CDK

$provision_kubectl_arm = <<PROVISION_KUBECTL_ARM
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/arm64/kubectl"
sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
PROVISION_KUBECTL_ARM

$provision_kubectl_intel = <<PROVISION_KUBECTL_INTEL
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
PROVISION_KUBECTL_INTEL

$provision_minikube_arm = <<PROVISION_MINIKUBE_ARM
# Ref https://minikube.sigs.k8s.io/docs/start/
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-arm64 -o minikube
sudo install minikube-linux-arm64 /usr/local/bin/minikube
rm minikube
PROVISION_MINIKUBE_ARM

$provision_minikube_intel = <<PROVISION_MINIKUBE_INTEL
# Ref https://minikube.sigs.k8s.io/docs/start/
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64 -o minikube
sudo install minikube-linux-amd64 /usr/local/bin/minikube
rm minikube-linux-amd64
PROVISION_MINIKUBE_INTEL

$provision_jmeter = <<PROVISION_JMETER
curl -o apache-jmeter-5.6.3.tgz https://dlcdn.apache.org//jmeter/binaries/apache-jmeter-5.6.3.tgz
tar xf apache-jmeter-5.6.3.tgz
rm apache-jmeter-5.6.3.tgz
echo 'export PATH=/home/vagrant/apache-jmeter-5.6.3/bin:${PATH}' >> /home/vagrant/.bash_profile
PROVISION_JMETER

$provision_zap = <<PROVISION_ZAP
curl -L https://github.com/zaproxy/zaproxy/releases/download/w2024-12-09/ZAP_WEEKLY_D-2024-12-09.zip -o ZAP_WEEKLY_D-2024-12-09.zip
unzip ZAP_WEEKLY_D-2024-12-09.zip
rm ZAP_WEEKLY_D-2024-12-09.zip
echo 'export PATH=/home/vagrant/ZAP_D-2024-12-09:${PATH}' >> /home/vagrant/.bash_profile
PROVISION_ZAP

Vagrant.configure("2") do |config|

  config.vm.define "arm" do |arm|
    arm.vm.box = $ARM_BOX

    arm.vm.provision "shell", reboot: false, inline: $provision_git
    arm.vm.provision "shell", reboot: false, inline: $provision_wget
    arm.vm.provision "shell", reboot: false, inline: $provision_unzip
    arm.vm.provision "shell", reboot: false, inline: $provision_docker
    arm.vm.provision "shell", reboot: false, inline: $provision_compose
    arm.vm.provision "shell", reboot: false, inline: $provision_java23_arm
    arm.vm.provision "shell", reboot: false, inline: $provision_maven
    arm.vm.provision "shell", reboot: false, inline: $provision_java_utils
    arm.vm.provision "shell", reboot: false, inline: $provision_nvm
    arm.vm.provision "shell", reboot: false, inline: $provision_node
    arm.vm.provision "shell", reboot: false, inline: $provision_aws_arm
    arm.vm.provision "shell", reboot: false, inline: $provision_cdk
    arm.vm.provision "shell", reboot: false, inline: $provision_hello_cdk
    arm.vm.provision "shell", reboot: false, inline: $provision_jmeter
    arm.vm.provision "shell", reboot: false, inline: $provision_zap
    arm.vm.provision "shell", reboot: false, inline: $provision_kubectl_arm
    arm.vm.provision "shell", reboot: false, inline: $provision_minikube_arm
end
  config.vm.define "intel" do |intel|
    intel.vm.box = $INTEL_BOX

    intel.vm.provision "shell", reboot: false, inline: $provision_git
    intel.vm.provision "shell", reboot: false, inline: $provision_wget
    intel.vm.provision "shell", reboot: false, inline: $provision_unzip
    intel.vm.provision "shell", reboot: false, inline: $provision_docker
    intel.vm.provision "shell", reboot: false, inline: $provision_compose
    intel.vm.provision "shell", reboot: false, inline: $provision_java23_intel
    intel.vm.provision "shell", reboot: false, inline: $provision_maven
    intel.vm.provision "shell", reboot: false, inline: $provision_java_utils
    intel.vm.provision "shell", reboot: false, inline: $provision_nvm
    intel.vm.provision "shell", reboot: false, inline: $provision_node
    intel.vm.provision "shell", reboot: false, inline: $provision_aws_intel
    intel.vm.provision "shell", reboot: false, inline: $provision_cdk
    intel.vm.provision "shell", reboot: false, inline: $provision_hello_cdk
    intel.vm.provision "shell", reboot: false, inline: $provision_jmeter
    intel.vm.provision "shell", reboot: false, inline: $provision_zap
    intel.vm.provision "shell", reboot: false, inline: $provision_kubectl_intel
    intel.vm.provision "shell", reboot: false, inline: $provision_minikube_intel
  end

  config.vm.hostname = "HyperDevOps-2024-December"
  config.vm.provider "virtualbox" do |v|
	# For running Minikube, uncomment the following two lines, and reboot using "vagrant up".
	#v.cpus = 2
    #v.memory = 4096
  end

  config.vm.network "forwarded_port", guest: 9000, host: 9000	# for SonarQube
  config.vm.network "forwarded_port", guest: 5601, host: 5601	# for Kibana
  config.vm.network "forwarded_port", guest: 1001, host: 6001	# for hello
  config.vm.network "forwarded_port", guest: 1002, host: 6002	# for wegood
  config.vm.network "forwarded_port", guest: 8080, host: 8080	# for Jenkins


end

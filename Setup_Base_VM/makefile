# Makefile to install all of the tools needed for the DevOpsForAgileCoaches training
# course in a Linux environment.

# Set this to the name of the package manager used in the target Linux system:
export INSTALL := yum
#export INSTALL := apt-get

# Set this to the home directory for the account that will be used to perform the
# course activities:
export USER_HOME := /home/$(echo ~)

# Set tool versions and paths:

DOCKER_COMPOSE_VERSION := 1.25.0
JDK_VERSION := jdk-14
JDK_URL := https://download.java.net/java/GA/jdk14/076bab302c7b4508975440c56f6cc26a/36/GPL/openjdk-14_linux-x64_bin.tar.gz
MAVEN_VERSION := 3.6.3
NODE_VERSION := v12.16.1

# Nothing below here should need to change -------------------------------------

export JAVA_HOME := $(USER_HOME)/jdk-14
export PATH := $(JAVA_HOME)/bin:$(PATH)
export PATH := $(USER_HOME)/apache-maven-3.6.3/bin:$(PATH)

export REMOTE := provision_git provision_docker provision_compose provision_java provision_maven provision_java_utils provision_hello_cdk provision_unzip provision_aws provision_node

.PHONY: remote $(REMOTE) local

default: remote

remote: $(REMOTE)

local:
	vagrant up

provision_git:
	sudo $(INSTALL) install -y git

provision_docker:
	sudo $(INSTALL) install -y $(INSTALL)-utils device-mapper-persistent-data lvm2
	sudo $(INSTALL)-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
	sudo $(INSTALL) install -y docker-ce docker-ce-cli containerd.io
	sudo systemctl start docker
	sudo usermod -aG docker $(USER)

provision_compose:
	sudo curl -L https://github.com/docker/compose/releases/download/$(DOCKER_COMPOSE_VERSION)/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
	sudo chmod +x /usr/local/bin/docker-compose

provision_java:
	wget $(JDK_URL)
	tar xvf openjdk-14_linux-x64_bin.tar.gz
	rm openjdk-14_linux-x64_bin.tar.gz
	echo "export JAVA_HOME=$(USER_HOME)/"$(JDK_VERSION) >> $(USER_HOME)/.bash_profile
	echo "export PATH="$(JAVA_HOME)"/bin:\${PATH}" >> $(USER_HOME)/.bash_profile
	sudo $(INSTALL) install -y java

provision_maven:
	wget http://apache.mirrors.hoobly.com/maven/maven-3/$(MAVEN_VERSION)/binaries/apache-maven-$(MAVEN_VERSION)-bin.tar.gz
	tar xvf apache-maven-$(MAVEN_VERSION)-bin.tar.gz
	rm apache-maven-$(MAVEN_VERSION)-bin.tar.gz
	echo "export PATH="$(USER_HOME)"/apache-maven-"$(MAVEN_VERSION)"/bin:\${PATH}" >> $(USER_HOME)/.bash_profile
	./apache-maven-$(MAVEN_VERSION)/bin/mvn -v
	mkdir -p $(USER_HOME)/.m2
	wget https://gitlab.com/cliffbdf/devops-1/-/raw/master/toolchain-vm.xml
	cp toolchain-vm.xml $(USER_HOME)/.m2/toolchains.xml

provision_java_utils:
	git clone https://gitlab.com/cliffbdf/utilities-java.git
	{ \
		cd utilities-java; \
		make install; \
		cd; \
	}

provision_hello_cdk:
	wget https://gitlab.com/cliffbdf/devops-7-hello/-/raw/master/cdk/target/hello.cdk-1.0.jar
	mvn install:install-file -Dfile=hello.cdk-1.0.jar -DgroupId=com.cliffberg.devopsforagilecoaches -DartifactId=hello.cdk -Dversion=1.0 -Dpackaging=jar

provision_unzip:
	sudo $(INSTALL) install -y unzip

provision_aws:
	curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
	unzip awscliv2.zip
	sudo ./aws/install

provision_node:
	wget https://nodejs.org/dist/$(NODE_VERSION)/node-$(NODE_VERSION)-linux-x64.tar.xz
	tar xvf node-$(NODE_VERSION)-linux-x64.tar.xz
	rm node-$(NODE_VERSION)-linux-x64.tar.xz
	echo "export PATH=$(USER_HOME)/node-"$(NODE_VERSION)"-linux-x64/bin:\${PATH}" >> $(USER_HOME)/.bash_profile

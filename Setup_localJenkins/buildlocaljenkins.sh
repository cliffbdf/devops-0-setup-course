pushd /home/vagrant
export JDK_DIR_NAME=jdk-23.0.1
export MAVEN_VERSION=3.9.9
cp /vagrant/devops-0-setup-course/Setup_localJenkins/Dockerfile .
cp /vagrant/devops-0-setup-course/Setup_localJenkins/jenkins.bash_profile .
cp /vagrant/devops-0-setup-course/Setup_localJenkins/toolchain-jenkins.xml .
cp /usr/bin/docker .
sudo docker build -t localjenkins --build-arg jdk_dir_name=$JDK_DIR_NAME --build-arg maven_version=$MAVEN_VERSION .
rm ./docker
popd
